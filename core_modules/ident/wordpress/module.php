<?php
//    Pastèque Web back office, WordPress ident module
//
//    Copyright (C) 2013-2015 Scil (http://scil.coop)
//      Cédric Houbart, Philippe Pary
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace WordPressID {
    require_once(\Pasteque\PT::$ABSPATH . "/core_modules/tools/wp_preprocessing.php");
    require_once(dirname(__FILE__) . "/config.php");
    \WordPress\loadWP($config['wordpress_base_path']);

    function log($user, $password) {
        $creds = array();
        $creds['user_login'] = $user;
        $creds['user_password'] = $password;
        $creds['remember'] = FALSE;
        $user = wp_signon($creds, FALSE);
        if (!is_wp_error($user)) {
            global $api_user;
            $api_user = $user;
            return $api_user->ID;
        } else {
            return null;
        }
    }
    function show_login() {
        auth_redirect();
    }

    function logout() {
        wp_logout();
    }
}

namespace Pasteque {
    function core_ident_login($user, $password) {
        return \WordPressID\log($user, $password);
    }
    function show_login_page() {
        return \WordPressID\show_login();
    }

    hook('logout', '\WordPressID\logout');
}
