<?php

namespace Pasteque;

class JWT {

    public $header;
    public $payload;
    public $signature;
    public $token;

    private static function b64urlenc($string) {
        return rtrim(strtr(base64_encode($string), '+/', '-_'), '=');
    }
    private static function b64urldec($string) {
        return base64_decode(str_pad(strtr($string, '-_', '+/'), strlen($string) % 4, '=', STR_PAD_RIGHT));
    }
    
    /** Simply build a token with everything computed.
     * Use build or decode to construct a JWT.
     * @param $header The decoded header associative array.
     * @param $payload The decoded payload associative array.
     * @param $signature The signature hash.
     * @param $token The raw base64 encoded token. */
    protected function __construct($header, $payload, $signature, $token) {
        $this->header = $header;
        $this->payload = $payload;
        $this->signature = $signature;
        $this->token = $token;
    }

    /** Build a token from it's data. */
    public static function build($header, $payload, $secret) {
        $algo = $header['alg'];
        $b64header = JWT::b64urlenc(json_encode($header));
        $b64payload = JWT::b64urlenc(json_encode($payload));
        $signBase = $b64header . '.' . $b64payload;
        switch ($algo) {
        case 'HS256': $signature = hash('sha256', $signBase . $secret); break;
        case 'HS512': $signature = hash('sha512', $signBase . $secret); break;
        // Error, unsupported, unknown algorithm or insecure token
        case 'RS256':
        case 'ES256':
        case 'none':
        default:
            Log::warn(sprintf('Invalid JWT hashing algorithm %s', $algo));
        return null;
        }
        $token = $signBase . '.' . $signature;
        return new JWT($header, $payload, $signature, $token);
    }

    /** Build a token object from the string token.
     * @return A decoded token if valid, null otherwise. */
    public static function decode($jwt, $secret) {
        $parts = explode('.', $jwt);
        if (count($parts) != 3) {
            // Error, this is not a jwt
            Log::warn(sprintf('Received an invalid JWT %s', $jwt));
            return null;
        }
        $signBase = $parts[0] . '.' . $parts[1];
        $header = json_decode(JWT::b64urldec($parts[0]), true);
        $payload = json_decode(JWT::b64urldec($parts[1]), true);
        $signature = $parts[2];
        $algo = $header["alg"];
        if ($header == null || $payload == null) {
            // The content is not a JSON string
            Log::warn('Invalid JWT payload or content');
            return null;
        }
        if (!isset($header['alg'])) {
            // The token doesn't have an "alg" attribute, reject it
            Log::warn('No alg found in JWT header');
            return null;
        }
        switch ($algo) {
        case 'HS256': $check = hash('sha256', $signBase . $secret); break;
        case 'HS512': $check = hash('sha512', $signBase . $secret); break;
        // Error, unsupported, unknown algorithm or insecure token
        case 'RS256':
        case 'ES256':
        case 'none':
        default:
            Log::warn(sprintf('Invalid JWT hashing algorithm %s', $algo));
            return null;
        }
        if ($check == $signature) {
            // This is a valid token, we can use it
            return new JWT($header, $payload, $signature, $jwt);
        } else {
            // Corrupted or fallacious token
            Log::warn('Rejected a corrupted or fallacious token');
            return null;
        }
    }

    /** Check if the token is valid from the "iat" field and expiration time.
     * We use iat + timeout instead of exp to prevent cracking a long time
     * token. */
    public function isValid($timeout) {
        if (isset($this->payload['iat'])) {
            $time = time();
            $exp = intVal($this->payload['iat']) + $timeout;
            return $exp > $time;
        } else {
            // No "issued at" date, consider to be invalid
            Log::debug('The token was rejected due to the absence of iat attribute');
            return false;
        }
    }

}
