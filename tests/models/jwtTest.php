<?php
//    Pasteque server testing
//
//    Copyright (C) 2012 Scil (http://scil.coop)
//
//    This file is part of Pasteque.
//
//    Pasteque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pasteque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pasteque.  If not, see <http://www.gnu.org/licenses/>.
namespace Pasteque;

require_once(dirname(dirname(__FILE__)) . "/common_load.php");

class JWTTest extends \PHPUnit_Framework_TestCase {

    public static function setUpBeforeClass() {
    }

    protected function tearDown() {
    }

    public static function tearDownAfterClass() {
    }

    
    public function testBuild() {
        $header = array("alg" => "HS512");
        $payload = array("test" => "content");
        $secret = "auienstldbepoldt";
        $jwt = JWT::build($header, $payload, $secret);
        $this->assertArrayHasKey("alg", $jwt->header,
                "alg key not found in header.");
        $this->assertEquals("HS512", $jwt->header["alg"],
                "alg key alterated in header.");
        $this->assertArrayHasKey("test", $jwt->payload,
                "test key not found in header.");
        $this->assertEquals("content", $jwt->payload["test"],
                "test key alterated in payload.");
        $this->assertEquals("eyJhbGciOiJIUzUxMiJ9.eyJ0ZXN0IjoiY29udGVudCJ9.f783d0c0cd11972b713956c241217021d745d0a33aefd83c95217e130fee6454762a0f93a8d2b35cc988ff153b39b6f2184587dc82d9974bc25fff9ea0ba0973", $jwt->token,
                "Invalid token value");
    }

    public function testDecode() {
        $token = "eyJhbGciOiJIUzUxMiJ9.eyJ0ZXN0IjoiY29udGVudCJ9.f783d0c0cd11972b713956c241217021d745d0a33aefd83c95217e130fee6454762a0f93a8d2b35cc988ff153b39b6f2184587dc82d9974bc25fff9ea0ba0973"; // see above
        $secret = "auienstldbepoldt";
        $jwt = JWT::decode($token, $secret);
        $this->assertFalse($jwt === null, "Unable to decode a valid token.");
        $this->assertEquals("HS512", $jwt->header["alg"]);
        $this->assertEquals("content", $jwt->payload["test"]);
    }

    /** @depends testBuild
     * @depends testDecode */
    public function testIsValidReject() {
        $header = array("alg" => "HS512");
        $payload = array("test" => "content");
        $secret = "auienstldbepoldt";
        $jwt = JWT::build($header, $payload, $secret);
        $this->assertFalse($jwt->isValid(100));
    }
    
    /** @depends testBuild
     * @depends testDecode */
    public function testIsValid() {
        $header = array("alg" => "HS512");
        $now = time() - 10; // Considered issued 10 seconds before
        $payload = array("iat" => $now);
        $secret = "auienstldbepoldt";
        $jwt = JWT::build($header, $payload, $secret);
        $this->assertTrue($jwt->isValid(20));
        $this->assertFalse($jwt->isValid(5));
    }
}